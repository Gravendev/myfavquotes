package fr.graven.myfavquotes.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import fr.graven.myfavquotes.models.quotes.UserQuoteDetails

class UserQuote {

    @SerializedName("tags")
    @Expose
    var tags: Array<String>? = null

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("favorites_count")
    @Expose
    var favoriteCount: Int = 0

    @SerializedName("dialogue")
    @Expose
    var dialogue: String? = null

    @SerializedName("favorite")
    @Expose
    var isFavorite: Boolean = false

    @SerializedName("author_permalink")
    @Expose
    var authorPermalink: String? = null

    @SerializedName("author")
    @Expose
    var author: String? = null

    @SerializedName("body")
    @Expose
    var body: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("user_details")
    @Expose
    var userDetails: UserQuoteDetails? = null


}
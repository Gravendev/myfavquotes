package fr.graven.myfavquotes.models.quotes

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import fr.graven.myfavquotes.models.UserQuote

class UserQuoteList {

    @SerializedName("page")
    @Expose
    var page: Int = 0

    @SerializedName("last_page")
    @Expose
    var isLastPage: Boolean = false

    @SerializedName("quotes")
    @Expose
    var quotes: Array<UserQuote>? = null

}
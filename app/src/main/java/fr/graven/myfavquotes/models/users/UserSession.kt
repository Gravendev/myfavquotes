package fr.graven.myfavquotes.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserSession {

    @SerializedName("User-Token")
    @Expose
    var userToken: String? = null

    @SerializedName("login")
    @Expose
    var login: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

}
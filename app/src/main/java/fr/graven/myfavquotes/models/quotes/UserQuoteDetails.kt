package fr.graven.myfavquotes.models.quotes

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import fr.graven.myfavquotes.models.UserQuote

class UserQuoteDetails {

    @SerializedName("favorite")
    @Expose
    var favorite : Boolean = false

}
package fr.graven.myfavquotes.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserProfile {

    @SerializedName("login")
    @Expose
    var login: String? = null

    @SerializedName("pic_url")
    @Expose
    var urlPicture: String? = null

    @SerializedName("public_favorites_count")
    @Expose
    var favoritesCount: Int = 0

    @SerializedName("followers")
    @Expose
    var followers: Int = 0

    @SerializedName("following")
    @Expose
    var following: Int = 0

    @SerializedName("pro")
    @Expose
    var isPro: Boolean = false

    @SerializedName("current_session")
    @Expose
    var session: UserSession? = null

}
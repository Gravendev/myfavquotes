package fr.graven.myfavquotes.models.users

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import fr.graven.myfavquotes.models.UserSession

class UserHolder {

    var user: UserSession? = null

}
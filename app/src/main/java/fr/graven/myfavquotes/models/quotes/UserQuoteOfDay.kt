package fr.graven.myfavquotes.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserQuoteOfDay {

    @SerializedName("qotd_date") @Expose
    var date: String? = null

    @SerializedName("quote")
    @Expose
    var quote: UserQuote? = null

}
package fr.graven.myfavquotes.utils

import android.graphics.BitmapFactory
import android.widget.ImageView
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL


class Tools {

    fun loadImageFromURL(fileUrl: String, iv: ImageView): Boolean {
        try {

            val myFileUrl = URL(fileUrl)
            val conn = myFileUrl.openConnection() as HttpURLConnection
            conn.doInput = true
            conn.connect()

            val `is` = conn.inputStream
            iv.setImageBitmap(BitmapFactory.decodeStream(`is`))

            return true

        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

}
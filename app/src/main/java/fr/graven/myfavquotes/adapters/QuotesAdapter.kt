package fr.graven.myfavquotes.adapters

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import fr.graven.myfavquotes.R
import fr.graven.myfavquotes.models.UserQuote
import java.util.*

class QuotesAdapter(private val context: Context, private var quotes: MutableList<UserQuote>, private var callback: LikeChangeCallback) : RecyclerView.Adapter<QuotesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_item_quote_adapter, parent, false)
        return ViewHolder(context, view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val label = quotes[position]
        val textBody = "\"" + label.body + "\""
        val textAuthor = label.author

        holder.textView.text = textBody
        holder.authorView.text = textAuthor

        /* load tags in recycler view */
        loadRecyclerView(holder.tagsRecyclerView, label.tags!!)

        /* hide layout for animation */
        holder.itemLayout.visibility = View.GONE

        if(label.userDetails != null)
        {
            if(label.userDetails!!.favorite) setLikeButton(holder, label) else setUnLikeButton(holder, label)
        }

        /* set onclick layout to show a dialog */
        holder.itemLayout.setOnClickListener {

            /* create dialog instance */
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.activity_item_quote_adapter)

            /* init views */
            val textBodyView = dialog.findViewById<TextView>(R.id.text)
            val textAuthorView = dialog.findViewById<TextView>(R.id.author)
            val tagRecyclerView = dialog.findViewById<RecyclerView>(R.id.recycler_view)
            val likeButtonView = dialog.findViewById<ImageButton>(R.id.like_button)

            loadRecyclerView(tagRecyclerView, label.tags!!)

            /* update values **/
            textBodyView.text = textBody
            textAuthorView.text = textAuthor
            likeButtonView.setOnClickListener {

                /* dismiss current dialog to avoid NPE */
                dialog.dismiss()

                /* swap like to unlike button */
                swapLikeButton(label, holder)

            }

            dialog.show()

        }

        /* set onclick like and unlike perform button callback */
        holder.likeButton.setOnClickListener {

            /* swap like to unlike button */
            swapLikeButton(label, holder)

        }

    }

    private fun loadRecyclerView(recyclerView: RecyclerView, tags : Array<String>) {
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = TagsAdapter(context, tags)
    }

    private fun swapLikeButton(userQuote: UserQuote, holder: ViewHolder) {

        if(!userQuote.isFavorite) setLikeButton(holder, userQuote) else setUnLikeButton(holder, userQuote)

        /* perform like or unlike callback */
        callback.onLikeChange(userQuote, userQuote.isFavorite)

    }

    private fun setLikeButton(holder: ViewHolder, userQuote: UserQuote) {
        val swapAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        holder.likeButton.startAnimation(swapAnimation)
        holder.likeButton.setImageResource(R.drawable.like)
        userQuote.isFavorite = true
    }

    private fun setUnLikeButton(holder: ViewHolder, userQuote: UserQuote) {

        val swapAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        swapAnimation.setAnimationListener(object : Animation.AnimationListener {

            override fun onAnimationStart(p0: Animation?) {}

            override fun onAnimationEnd(p0: Animation?) {
                /* remove quote */
                var pos = holder.adapterPosition
                quotes.removeAt(pos)
                notifyItemRemoved(pos)
                callback.onUnlikeSuccess(itemCount)
            }

            override fun onAnimationRepeat(p0: Animation?) {}

        })
        holder.likeButton.startAnimation(swapAnimation)
        holder.likeButton.setImageResource(R.drawable.unlike)
        userQuote.isFavorite = false
    }

    override fun getItemCount(): Int {
        return quotes!!.size
    }

    class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemLayout = itemView
        var textView  = itemView.findViewById<TextView>(R.id.text)
        var authorView = itemView.findViewById<TextView>(R.id.author)
        var tagsRecyclerView : RecyclerView = itemView.findViewById(R.id.recycler_view)
        var likeButton = itemView.findViewById<AppCompatImageButton>(R.id.like_button)

        /* delayed appear animation */
        val appearAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_up)
        val random = Random()

        init {
            Handler().postDelayed({
                itemLayout.visibility = View.VISIBLE
                this.itemLayout.startAnimation(appearAnimation)
            }, (random.nextInt(5000) / 3).toLong())
        }


    }

    interface LikeChangeCallback {

        fun onLikeChange(userQuote: UserQuote, isLike: Boolean)

        fun onUnlikeSuccess(favCount : Int)

    }

}
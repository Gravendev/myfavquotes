package fr.graven.myfavquotes.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView

class FavTextView : TextView {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setTypeface()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setTypeface()
    }

    constructor(context: Context) : super(context) {
        setTypeface()
    }

    private fun setTypeface() {
        super.setTypeface(Typeface.createFromAsset(super.getContext().assets, "fonts/fav_italic_font.ttf"))
    }

}
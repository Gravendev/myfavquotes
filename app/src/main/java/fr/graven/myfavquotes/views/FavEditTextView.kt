package fr.graven.myfavquotes.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText
import android.widget.TextView

class FavEditTextView : EditText {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setTypeface()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setTypeface()
    }

    constructor(context: Context) : super(context) {
        setTypeface()
    }

    private fun setTypeface() {
        val typeface = typeface

        if (typeface != null) {
            val fontName = if (typeface.isBold) "fav_bold_font" else "fav_font"
            super.setTypeface(Typeface.createFromAsset(super.getContext().assets, "fonts/$fontName.ttf"))
        }

    }

}
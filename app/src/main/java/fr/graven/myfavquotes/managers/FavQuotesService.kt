package fr.graven.myfavquotes.managers

import fr.graven.myfavquotes.models.UserProfile
import fr.graven.myfavquotes.models.UserQuote
import fr.graven.myfavquotes.models.UserSession
import fr.graven.myfavquotes.models.quotes.UserQuoteList
import fr.graven.myfavquotes.models.users.UserHolder
import retrofit2.Call
import retrofit2.http.*

interface FavQuotesService {

    @POST("/api/session")
    fun createSession(@Body session: UserHolder, @Header("Authorization") auth_token: String): Call<UserSession>

    @PUT("/api/quotes/{quote_id}/unfav")
    fun unLike(@Path("quote_id") quoteId: Int?, @Body quote: UserQuote, @Header("Authorization") auth_token: String, @Header("User-Token") token: String?): Call<UserQuote>

    @GET("/api/quotes")
    fun getQuotes(@Query("filter") filter: String, @Query("type") type: String, @Header("Authorization") auth_token: String?, @Header("User-Token") token: String?): Call<UserQuoteList>

    @GET("/api/users/{login}")
    fun getUserByLogin(@Path("login") login: String?, @Header("Authorization") auth_token: String?, @Header("User-Token") token: String?): Call<UserProfile>


}
package fr.graven.myfavquotes.managers

import android.util.Log
import fr.graven.myfavquotes.activities.MainActivity
import fr.graven.myfavquotes.models.UserProfile
import fr.graven.myfavquotes.models.UserSession
import fr.graven.myfavquotes.models.users.UserHolder
import fr.graven.myfavquotes.models.UserQuote
import fr.graven.myfavquotes.models.quotes.UserQuoteDetails
import fr.graven.myfavquotes.models.quotes.UserQuoteList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavServicesManager {

    /* main service */
    var service: FavQuotesService? = null
    var userProfile: UserProfile? = null

    /* temp app data */
    private var authToken : String? = null

    companion object {
        const val TAG = "FavLoginManager"
    }

    init {

        /* init FavQ API main service */
        service = FavServiceGenerator().createService()

    }

    fun setAuthToken(authTokenParam : String) {
       authToken = "Token token=\"$authTokenParam\""
    }

    fun loginAttempt(userSession: UserSession, callback: LoginCallback) {

        /* create a user session holder who contain this user session */
        val userSessionHolder = UserHolder()
        userSessionHolder.user = userSession

        /* attempt login session call */
        service?.createSession(userSessionHolder, authToken!!)?.enqueue(object : Callback<UserSession> {

            override fun onResponse(call: Call<UserSession>, response: Response<UserSession>) {
                Log.d(MainActivity.TAG, "onResponse($response) ")

                /* attempt get user session call success */
                getUserFromSession(response.body(), callback)

            }

            override fun onFailure(call: Call<UserSession>, t: Throwable) {
                Log.d(TAG, "onFailure1({${t.cause}})")
            }
        })

    }

    fun unLike(userProfile: UserProfile, userQuote: UserQuote) {

        val userSession = userProfile?.session
        userQuote.userDetails = UserQuoteDetails()
        userQuote.userDetails!!.favorite = false

        service?.unLike(userQuote.id, userQuote, authToken!!, userSession?.userToken)?.enqueue(object  : Callback<UserQuote> {

            override fun onResponse(call: Call<UserQuote>?, response: Response<UserQuote>?) {
                Log.d(TAG, "onResponse(${call!!.request().url()} $response) ")
            }

            override fun onFailure(call: Call<UserQuote>?, t: Throwable?) { Log.d(TAG, "onFailure()") }

        })

    }

    fun getQuotes(userProfile: UserProfile, callback: QuoteCallback) {

        val userSession = userProfile?.session

        service?.getQuotes(userProfile.login!!, "user", authToken, userSession?.userToken)?.enqueue(object : Callback<UserQuoteList> {

            override fun onResponse(call: Call<UserQuoteList>, response: Response<UserQuoteList>) {
                Log.d(TAG, "onResponse(${call.request().url()} $response) ")

                /* call back the quotes list for this user */
                callback.onRecieveList(response.body())

            }

            override fun onFailure(call: Call<UserQuoteList>, t: Throwable) {
                Log.d(TAG, "onFailure1({${t.cause}})")
            }
        })

    }

    private fun getUserFromSession(userSession: UserSession, callback: LoginCallback){

        /* create get user by login call*/
        val userCall = service?.getUserByLogin(userSession.login, authToken, userSession.userToken)

        /* attempt call to get user profile */
        userCall?.enqueue(object : Callback<UserProfile> {

            override fun onResponse(call: Call<UserProfile>, response: Response<UserProfile>) {

                /* get user profile response body */
                val user = response.body()

                /* assign user session to the profile */
                user.session = userSession

                /* assign/store the user profile */
                userProfile = user

                /* perform onLoginSuccess() callback */
                callback.onLoginSuccess(user)

                /* successful connected ! */
                Log.d(TAG, "Successful connected to FavQs API as '${user.login}'")

            }

            override fun onFailure(call: Call<UserProfile>, t: Throwable) {

                /* perform onLoginFailed() callback */
                callback.onLoginFailed()

            }

        })



    }

    fun getCurrentUserProfile() : UserProfile? {
        return userProfile
    }

}

interface LoginCallback {

    fun onLoginSuccess(userProfile: UserProfile)

    fun onLoginFailed()

}

interface QuoteCallback {

    fun onRecieveList(quotesList: UserQuoteList)

}

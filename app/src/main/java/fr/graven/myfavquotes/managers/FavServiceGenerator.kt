package fr.graven.myfavquotes.managers

import fr.graven.myfavquotes.utils.Tls12SocketFactory
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class FavServiceGenerator() {

    companion object {
        const val END_POINT = "https://favqs.com/api/"
    }

    fun createService(): FavQuotesService {

        /* create retrofit builder */
        val retrofit = Retrofit.Builder()
                .baseUrl(END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build()

        /* return retrofit FavQuotes service */
        return retrofit.create<FavQuotesService>(FavQuotesService::class.java)

    }

    private fun getClient(): OkHttpClient? {

        /* create okHttp client request */
        val client = OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)

        /* setup TLS protocol */
        try {
            val trustManager = createTrustManager()
            val sc = SSLContext.getInstance("TLSv1.2")
            sc.init(null, arrayOf<TrustManager>(trustManager!!), null)
            val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).tlsVersions(TlsVersion.TLS_1_2).build()
            client.sslSocketFactory(Tls12SocketFactory(sc.socketFactory), trustManager)
            client.connectionSpecs(listOf<ConnectionSpec>(cs))

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        }

        return client.build()

    }

    private fun createTrustManager(): X509TrustManager? {

        var trustManager: X509TrustManager? = null

        try {
            val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)
            val trustManagers = trustManagerFactory.trustManagers
            if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
                throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
            }
            trustManager = trustManagers[0] as X509TrustManager
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return trustManager

    }

}


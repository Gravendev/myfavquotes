package fr.graven.myfavquotes.managers

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import fr.graven.myfavquotes.models.UserProfile
import fr.graven.myfavquotes.models.quotes.UserQuoteList
import com.google.gson.Gson
import fr.graven.myfavquotes.R
import fr.graven.myfavquotes.models.UserSession

class MyPreferencesManager(private val context: Context) {

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)

    fun saveInCacheData(userProfile: UserProfile, quotesList: UserQuoteList) {

        Log.d(TAG, "saveInCacheData()")

        /* init preference editor */
        val prefsEditor = preferences.edit()
        val gson = Gson()

        /* save user profile data */
        prefsEditor.putString(USER_PROFILE, gson.toJson(userProfile))

        /* save user quotes list data */
        prefsEditor.putString(USER_QUOTES_LIST, gson.toJson(quotesList))

        /* notify update with a single commit */
        prefsEditor.commit()

    }

    fun saveLoginData(login: String, password: String){

        Log.d(TAG, "saveLoginData()")

        /* init preference editor */
        val prefsEditor = preferences.edit()

        /* save user session data */
        prefsEditor.putString(USER_LOGIN_SESSION, login)
        prefsEditor.putString(USER_PASSWORD_SESSION, password)

        /* notify update with a single commit */
        prefsEditor.commit()

    }

    fun getUserProfile() : UserProfile {
        Log.d(TAG, "getUserProfile()")
        val json = preferences.getString(USER_PROFILE, "")
        return Gson().fromJson<UserProfile>(json, UserProfile::class.java!!)
    }

    fun getUserQuotesList() : UserQuoteList {
        Log.d(TAG, "getUserQuotesList()")
        val json = preferences.getString(USER_QUOTES_LIST, "")
        return Gson().fromJson<UserQuoteList>(json, UserQuoteList::class.java!!)
    }

    fun getUserSession() : UserSession {

        Log.d(TAG, "getUserSession()")

        /* generate new user session object */
        val userSession = UserSession()
        userSession.login = getSessionKey(USER_LOGIN_SESSION, R.string.default_login_session)
        userSession.password = getSessionKey(USER_PASSWORD_SESSION, R.string.default_login_password)

        /* return this session object */
        return userSession

    }

    private fun getSessionKey(key: String, defaultResourceId: Int) : String{
        return preferences.getString(key, context.resources.getString(defaultResourceId))
    }

    companion object {
        val TAG = "PreferencesManager"
        private val USER_PROFILE = "userProfile"
        private val USER_QUOTES_LIST = "userQuotesList"
        private val USER_LOGIN_SESSION = "userLoginSession"
        private val USER_PASSWORD_SESSION = "userPassSession"
    }

}
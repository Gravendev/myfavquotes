package fr.graven.myfavquotes.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import fr.graven.myfavquotes.R
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.squareup.picasso.Picasso
import fr.graven.myfavquotes.adapters.QuotesAdapter
import fr.graven.myfavquotes.managers.FavServicesManager
import fr.graven.myfavquotes.managers.LoginCallback
import fr.graven.myfavquotes.managers.QuoteCallback
import fr.graven.myfavquotes.models.UserProfile
import fr.graven.myfavquotes.models.UserQuote
import android.net.ConnectivityManager
import android.util.Log
import android.widget.*
import fr.graven.myfavquotes.models.quotes.UserQuoteList
import fr.graven.myfavquotes.managers.MyPreferencesManager

class MainActivity : AppCompatActivity() {

    var servicesManager: FavServicesManager? = null
    var preferencesManager : MyPreferencesManager? = null
    var context: Context = this

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quotes)

        /* init tool bar */
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        /* init service */
        servicesManager = FavServicesManager()
        servicesManager!!.setAuthToken(resources.getString(R.string.api_key))
        preferencesManager = MyPreferencesManager(context)

        /* attempt login */
        login()

    }

    /* UI */

    @SuppressLint("RestrictedApi")
    private fun updateUI() {

        /* check internet connection */
        val hasInternet = isOnline()
        var userProfile = servicesManager?.getCurrentUserProfile()

        /* get the full quotes list for this user on internet enabled */
        if(hasInternet && userProfile != null)
        {
            /* setup image_view profile (from url) (back and front) */
            val imageProfileViewBack = findViewById<ImageView>(R.id.profile_picture_back)
            Picasso.with(baseContext).load(userProfile!!.urlPicture).into(imageProfileViewBack)

            /* retrieve user quotes data */
            servicesManager!!.getQuotes(userProfile, object : QuoteCallback {

                override fun onRecieveList(quotesList: UserQuoteList) {

                    /* load recycler view with adapter */
                    loadQuotes(userProfile!!, quotesList!!)

                }

            })

        }

        /* get the full quotes list for this user on internet disabled */
        else
        {

            /* retrieve user profile data */
            userProfile = preferencesManager?.getUserProfile()
            servicesManager?.userProfile = userProfile

            /* retrieve user quotes data */
            loadQuotes(userProfile!!, preferencesManager?.getUserQuotesList()!!)

        }

        /* set username profile */
        val userNameView = findViewById<TextView>(R.id.username_text)
        userNameView.text = userProfile?.login

        /* setup refresh button */
        val refreshButton = findViewById<FloatingActionButton>(R.id.refresh_button)
        refreshButton.visibility = View.GONE

    }

    private fun displayLoginDialog() {

        /* create dialog instance */
        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.login_popup_dialog)

        /* manage on click listeners */
        val submitButton = dialog.findViewById<Button>(R.id.submit_button)
        submitButton.setOnClickListener {

            /* save input text value in preferences */
            val loginText = dialog.findViewById<EditText>(R.id.login_input_text).text.toString()
            val passwordText = dialog.findViewById<EditText>(R.id.password_input_text).text.toString()
            preferencesManager?.saveLoginData(loginText, passwordText)

            /* dismiss current login dialog */
            dialog.dismiss()

            /* re-try login callback */
            login()

        }

        /* show it */
        dialog.show()

    }

    fun onRefreshQuotes(refreshButton : View){

        /* create rotate animation */
        val rotateAnimation = AnimationUtils.loadAnimation(context, R.anim.rotate)
        rotateAnimation.setAnimationListener(object : Animation.AnimationListener {

            override fun onAnimationStart(p0: Animation?) { updateUI()  }

            override fun onAnimationEnd(p0: Animation?) { refreshButton.isClickable = true }

            override fun onAnimationRepeat(p0: Animation?) {}

        })

        /* start this animation */
        refreshButton.startAnimation(rotateAnimation)
    }

    fun updateQuoteCount(quotesNumber : Int) {

        /* set quotes number profile */
        val countNameView = findViewById<TextView>(R.id.favs_count_text)
        countNameView.text = resources.getQuantityString(R.plurals.profile_quotes_numbers, quotesNumber, quotesNumber)

    }

    @SuppressLint("RestrictedApi")
    fun loadQuotes(userProfile : UserProfile, quotesList: UserQuoteList) {

        /* load recycler view with adapter */
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = QuotesAdapter(context, quotesList.quotes!!.toMutableList()!!, object : QuotesAdapter.LikeChangeCallback {

            override fun onLikeChange(userQuote: UserQuote, isLike: Boolean) {

                /* unlike the quote on the API*/
                if (!isLike)
                    servicesManager!!.unLike(userProfile, userQuote)

            }

            override fun onUnlikeSuccess(updatedfavCount: Int) {

                /* update favs count view */
                updateQuoteCount(updatedfavCount)

            }

        })

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.isNestedScrollingEnabled = false

        updateQuoteCount(quotesList.quotes!!.size)

        /* show hided refresh button */
        val refreshButton = findViewById<FloatingActionButton>(R.id.refresh_button)
        refreshButton.visibility = View.VISIBLE

        /* save datas */
        preferencesManager?.saveInCacheData(userProfile, quotesList)

    }

    /* LOGIN / CONNECTIVITY */

    private fun login(){

        /* get users data */
        val userSession = preferencesManager?.getUserSession()

        /* if user has an internet connection */
        if(isOnline())
        {
            /* attempt login */
            servicesManager!!.loginAttempt(userSession!!, object : LoginCallback {

            override fun onLoginSuccess(userProfile: UserProfile) {
                Toast.makeText(applicationContext, "Connected successfully as '${userProfile.login}' !", Toast.LENGTH_SHORT).show()

                /* update ui and load all components for this user */
                updateUI()

            }

            override fun onLoginFailed() {

                /* re-try connection with a new login dialog  */
                displayLoginDialog()

                /* alert the user with a toast */
                val login = userSession.login
                Toast.makeText(applicationContext, "Connection failed for $login", Toast.LENGTH_SHORT).show()

            }

            })

        }

        else
            /* update ui and load all components for this offline user */
            updateUI()

    }

    private fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

}
